package sample;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

import static sample.Main.main;

public class Controller implements Initializable
{
    boolean isServer;
    private String IP;
    private int port;
    private String Login;

    private NetworkConnection connection;

    //public static Controller controller;
    //@FXML object

    @FXML Button Send;
    @FXML TextField msgField;


    @FXML TextArea msgArea;


    @FXML Label portLabel;
    @FXML TextField portField;
    @FXML Label ipLabel;
    @FXML TextField IpField;
    @FXML Label nameLabel;
    @FXML TextField nameField;
    @FXML Button loginButton;
    @FXML RadioButton clientChoice;
    @FXML RadioButton serverChoice;


    //@FXML functions
    @FXML void setType()
    {
        if(serverChoice.isSelected()) {
            IpField.setDisable(true);
            isServer = true;
            System.out.println(isServer);
        }
        else if(clientChoice.isSelected())
        {
            IpField.setDisable(false);
            isServer = false;
            System.out.println(isServer);
        }
    }

    @FXML void setAttributes()
    {
        if(isServer)
        {
            this.port = Integer.parseInt(portField.getText());
            this.Login = nameField.getText();
            System.out.println(port + " " + Login);
        }
        else if(!isServer)
        {
            this.IP = IpField.getText();
            this.port = Integer.parseInt(portField.getText());
            this.Login = nameField.getText();
            System.out.println(IP + " " + port + " " + Login);
        }
    }

    @FXML void connect() throws Exception {
        if(isServer)
        {
            connection = new Server(this.getPort(),data ->{
                Platform.runLater(()->{
                    msgArea.appendText(data.toString() + "\n");
                });
            });
            connection.startConnection();
        }
        else
        {
            connection = new Client(this.getIP(),this.getPort(),data ->{
                Platform.runLater(()->{
                    msgArea.appendText(data.toString() + "\n");
                });
            });
            connection.startConnection();
        }
    }
    @FXML void Disconnect() throws Exception {
        connection.closeConnection();
    }

   @FXML void Send() {
        String message = isServer ? "Server: " : "Client: ";
        message += msgField.getText();
        msgField.clear();

        msgArea.appendText(message + "\n");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public String getIP()
    {
      return IP;
    }
    public String getLogin()
    {
      return Login;
    }
    public int getPort()
    {
       return port;
    }

}
